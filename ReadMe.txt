An Investigation into Improving Speech Enhancement by Implementing a Deep Cascade Learning Neural Network

Introduction
The aim of the project is to create and deep cascade learning model that will be able to reduce the noise 
in speech whilst also trying to reduce the time complexity and memory allocation needed used by other models 
already available.

Requirement
-> numpy 1.16.4
-> keras 2.2.4
-> SpeechRecognition 3.8.1
-> Tensorflow 1.14.0
-> scipy 1.3.0

Usage
The follows code can be found in the Scipts folder:
-> End-to-End - used for end-to-end training for the first dataset
-> End-to-End2 - used for transfer learning on end-to-end trained model for the second dataset
-> Model - contains the model for the end-to-end training
-> Deep-Cascade-Learning - used for deep cascade learning training for the first dataset
-> Deep-Cascade-Learning2 - used for transfer learning on deep cascade learning trained model for the second dataset
-> Preprocessing - used to pre-process and post-process the data
-> Speech-to-Text - used to convert audio to text data

The follows code can be found in the python-pesq/tests folder:
-> test_pesq - used to evaluate the PESQ score

The follows code can be found in the Speech_to_Text folder:
-> wer - used to evaluate the WER score