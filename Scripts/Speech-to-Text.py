import speech_recognition as sr
r = sr.Recognizer()

hellow=sr.AudioFile("audiofile.wav") #enter path of audio file to be converted to text
with hellow as source:
    audio = r.record(source)
try:
    s = r.recognize_google(audio)
    print("Text: "+s)
    f= open("audiofile.txt","w+") #enter name of new txt file
    f.write(s)
    f.close()
except Exception as e:
    print("Exception: "+str(e))

