# This code was referenced from below.
# https://github.com/FrankLongueira/Thesis/tree/master/Scripts


import numpy as np
import scipy.signal
import scipy.io.wavfile
import math

def load(folder, filename):
	file_path = folder + filename
	fs, audio = scipy.io.wavfile.read(file_path)
	return audio, fs

def combine(audio, noise, snr_level):
	if( audio.size >= noise.size ):
		audio = audio[0:noise.size]
	else:
		noise = noise[0:audio.size]

	audio = audio.astype('float')
	noise = noise.astype('float')

	audio_mean = np.mean(np.power(np.absolute(audio),2))
	noise_mean = np.mean(np.power(np.absolute(noise),2))
	target = audio_mean / (10**(snr_level/10.))

	scaling_coeff = np.sqrt(target) / np.sqrt(noise_mean)

	noisy_data = audio + (scaling_coeff * noise)

	return(noisy_data)

def apply_window(frame):

	M = frame.size
	window = scipy.signal.hann(M)

	return(frame*window)

def generate_frames(audio, fs, frame_time, lag = 0.5 ):
	frame_length = int(math.ceil(( frame_time * fs )   / 2.) * 2)
	total_time_steps = int((audio.size / (frame_length * lag)) - 1)
	x_train = np.zeros( shape = (frame_length, total_time_steps) )

	for i in range(0, (total_time_steps - 1)):
		x_train[:, i] =  apply_window( audio[ int(i*(frame_length / 2)) : int((i*(frame_length / 2) + frame_length)) ] )

	return(x_train)

def scale_features(features, mean, std):

	features = (features - mean) / float(std)

	return(features.T)

def generate(audio, fs, frame_time, mean, std):

	x_frames = generate_frames(audio, fs, frame_time,)
	x_frames_scaled = scale_features( x_frames, mean, std)
	x_frames_scaled_input = np.reshape(x_frames_scaled, (x_frames_scaled.shape[0], x_frames_scaled.shape[1], 1))

	return(x_frames_scaled_input)

def overlapp_add_reconstruction(frame1_windowed, frame2_windowed):

	M = frame2_windowed.size
	R = M//2
	output = np.zeros(frame1_windowed.size + R)
	output[:frame1_windowed.size] = frame1_windowed
	output[(frame1_windowed.size-R):] = output[(frame1_windowed.size-R):] + frame2_windowed

	return(output)

def reconstruct(test):
	output = test[0, :]
	for i in range(1, test.shape[0]-1):
		output = overlapp_add_reconstruction(output, test[i, :])

	return(output.astype('int16'))

