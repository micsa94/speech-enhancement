#import libraries
import numpy as np
import os
import scipy.io.wavfile

#import scripts
import Preprocessing as pro
import Model as cnn

snr_db = 5
frame_time = 0.020
epochs = 40
batch_size = 100
filter_size_per_hidden_layer = [0.005, 0.005, 0.005, 0.005, 0.005]
filter_size_output_layer = 0.005
num_filters_per_hidden_layer = [12, 25, 50, 100, 200] # model 1
# num_filters_per_hidden_layer = [25, 25, 50, 50, 100] # model 2
patience = 20
model_name = "Model_1"

#audio paths
print("Main path of audio files")
cwd = os.getcwd()
parent_cwd = os.path.abspath(os.path.join(cwd, os.pardir))
audio_folder = parent_cwd + "/Audio_Files/"

#training data
train_clean_time_series,sampling_rate = pro.load( audio_folder, filename = "train.wav")
train_noise_time_series, sampling_rate = pro.load( audio_folder, filename = "train_noise.wav")
train_noisy_time_series = pro.combine(train_clean_time_series, train_noise_time_series, snr_db)
train_mean = np.mean(train_clean_time_series)
train_standard_deviation = np.std(train_clean_time_series)
train_clean = pro.generate(train_clean_time_series, sampling_rate, frame_time, train_mean, train_standard_deviation )
train_noisy = pro.generate(train_noisy_time_series, sampling_rate, frame_time, train_mean, train_standard_deviation )

#validation data
valid_clean_time_series,sampling_rate  = pro.load( audio_folder, filename = "valid.wav")
valid_noise_time_series, sampling_rate = pro.load( audio_folder, filename = "valid_noise.wav")
valid_noisy_time_series = pro.combine(valid_clean_time_series, valid_noise_time_series, snr_db)
validation_clean = pro.generate(valid_clean_time_series, sampling_rate, frame_time, train_mean, train_standard_deviation)
validation_noisy = pro.generate(valid_noisy_time_series, sampling_rate, frame_time, train_mean, train_standard_deviation)

#test data
test_clean_time_series,sampling_rate = pro.load( audio_folder, filename = "test.wav")
test_noise_time_series, sampling_rate = pro.load( audio_folder, filename = "test_noise.wav")
test_noisy_time_series = pro.combine(test_clean_time_series, test_noise_time_series, snr_db)
test_clean = pro.generate(test_clean_time_series, sampling_rate, frame_time, train_mean, train_standard_deviation)
test_noisy = pro.generate(test_noisy_time_series, sampling_rate, frame_time, train_mean, train_standard_deviation)
scipy.io.wavfile.write( filename = parent_cwd + "/Audio_Files/Test_Files/" + "Test_Clean_" + str(snr_db) + "dB.wav", rate = sampling_rate, data = test_clean_time_series.astype('int16')[0:(60*sampling_rate)])
scipy.io.wavfile.write( filename = parent_cwd + "/Audio_Files/Test_Files/" + "Test_Noisy_" + str(snr_db) + "dB.wav", rate = sampling_rate, data = test_noisy_time_series.astype('int16')[0:(60*sampling_rate)])

print("Training Neural Network")
input_shape = (train_noisy.shape[1], 1)

model_save_path = parent_cwd + "/Saved_Models/" + model_name
# model = cnn.load_model_(model_save_path)
model = cnn.cnn_model(input_shape, num_filters_per_hidden_layer, list(map(int, np.array(filter_size_per_hidden_layer)*sampling_rate)), int(filter_size_output_layer*sampling_rate) )
model, history = cnn.train_model( 	model = model,
									train_inputs = train_noisy,
									train_labels = train_clean,
									epochs = epochs,
									batch_size = batch_size,
									validation_inputs = validation_noisy,
									validation_labels = validation_clean,
									filepath = model_save_path,
									patience = patience)

print("Getting CNN output for noisy test set input...")
test_filtered_frames = (train_standard_deviation * cnn.get_output_multiple_batches(model, test_noisy)) + train_mean

print("Perfectly reconstructing filtered test set audio & saving to memory...")
test_filtered = pro.reconstruct( test_filtered_frames )
scipy.io.wavfile.write( filename = parent_cwd + "/Audio_Files/Test_Files/" + model_name + "_FilteredTest_" + str(snr_db) + "dB_1min.wav", rate = sampling_rate, data = test_filtered[0:(60*sampling_rate)])

summary_stats_filename = parent_cwd + "/Saved_Models/Model_Overview.txt"
cnn.summary_statistics( summary_stats_filename, model_name, frame_time, snr_db,
						 	num_filters_per_hidden_layer, filter_size_per_hidden_layer, filter_size_output_layer,
						 	epochs, batch_size)
