#import libraries
import numpy as np
import os
import scipy.io.wavfile

#import scripts
import Preprocessing as pro
import Model as cnn
from keras.models import Sequential
from keras.layers import Conv1D, PReLU
from keras.layers.normalization import BatchNormalization

snr_db = 5
frame_time = 0.020
epochs = 40
batch_size = 100
filter_size_per_hidden_layer = [0.005, 0.005, 0.005, 0.005, 0.005]
filter_size_output_layer = 0.005
num_filters_per_hidden_layer = [12, 25, 50, 100, 200] # model 1
# num_filters_per_hidden_layer = [25, 25, 50, 50, 100] # model 2
patience = 20
model_name = "Model_1"

#audio paths
print("Main path of audio files")
cwd = os.getcwd()
parent_cwd = os.path.abspath(os.path.join(cwd, os.pardir))
audio_folder = parent_cwd + "/Audio_Files/"

#training data
train_clean_time_series,sampling_rate = pro.load( audio_folder, filename = "Chapter_1.wav")
train_noise_time_series, sampling_rate = pro.load( audio_folder, filename = "Chapter_1_Bar_Noise.wav")
train_noisy_time_series = pro.combine(train_clean_time_series, train_noise_time_series, snr_db)
train_mean = np.mean(train_clean_time_series)
train_standard_deviation = np.std(train_clean_time_series)
train_clean = pro.generate(train_clean_time_series, sampling_rate, frame_time, train_mean, train_standard_deviation )
train_noisy = pro.generate(train_noisy_time_series, sampling_rate, frame_time, train_mean, train_standard_deviation )
train_noisy= np.pad(train_noisy, ((0,53), (0,0), (0, 0)), 'constant')

#validation data
valid_clean_time_series,sampling_rate  = pro.load( audio_folder, filename = "Chapter_2.wav")
valid_noise_time_series, sampling_rate = pro.load( audio_folder, filename = "Chapter_2_Cafe_Noise.wav")
valid_noisy_time_series = pro.combine(valid_clean_time_series, valid_noise_time_series, snr_db)
validation_clean = pro.generate(valid_clean_time_series, sampling_rate, frame_time, train_mean, train_standard_deviation)
validation_noisy = pro.generate(valid_noisy_time_series, sampling_rate, frame_time, train_mean, train_standard_deviation)

#test data
test_clean_time_series,sampling_rate = pro.load( audio_folder, filename = "Chapter_3.wav")
test_noise_time_series, sampling_rate = pro.load( audio_folder, filename = "Chapter_3_Cafe_Noise.wav")
test_noisy_time_series = pro.combine(test_clean_time_series, test_noise_time_series, snr_db)
test_clean = pro.generate(test_clean_time_series, sampling_rate, frame_time, train_mean, train_standard_deviation)
test_noisy = pro.generate(test_noisy_time_series, sampling_rate, frame_time, train_mean, train_standard_deviation)
scipy.io.wavfile.write( filename = parent_cwd + "/Audio_Files/Test_Files/" + "Test_Clean_" + str(snr_db) + "dB.wav", rate = sampling_rate, data = test_clean_time_series.astype('int16')[0:(60*sampling_rate)])
scipy.io.wavfile.write( filename = parent_cwd + "/Audio_Files/Test_Files/" + "Test_Noisy_" + str(snr_db) + "dB.wav", rate = sampling_rate, data = test_noisy_time_series.astype('int16')[0:(60*sampling_rate)])

print("Training Neural Network")
input_shape = (train_noisy.shape[1], 1)

def get_base_model(train_noisy, train_clean, validation_noisy, validation_clean):
	# define model
	model = Sequential()
	model.add(Conv1D(filters = 25, kernel_size = 80, padding='same', input_shape = input_shape))
	model.add(BatchNormalization())
	model.add(PReLU())
	model.add(Conv1D(1, kernel_size = 80, padding='same'))
	print(model.summary())
	# compile model
	model.compile(loss='mean_squared_error', optimizer='adam')
	# fit model
	model.fit(	train_noisy, train_clean,
            				epochs = epochs,
                			batch_size = batch_size,
                			shuffle = True,
                			validation_data = (validation_noisy, validation_clean))
	return model


# get the base model
model = get_base_model(train_noisy, train_clean, validation_noisy, validation_clean)
# evaluate the base model

# add one new layer and re-train only the new layer
def add_layer(i, num_filters_per_hidden_layer, model, train_noisy, train_clean, validation_noisy, validation_clean):
	# remove the output layer
	model.pop()
	model.add(Conv1D(filters =num_filters_per_hidden_layer[i+1], kernel_size = 80, padding='same'))
	model.add(BatchNormalization())
	model.add(PReLU())
	# re-add the output layer
	model.add(Conv1D(1, kernel_size = 80, padding='same'))
	for layer in model.layers[:-4]:
		layer.trainable = False
	for layer in model.layers:
		print(layer, layer.trainable)
	print(model.summary())
	# fit model
	model.fit(	train_noisy, train_clean,
            				epochs = epochs,
                			batch_size = batch_size,
                			shuffle = True,
                			validation_data = (validation_noisy, validation_clean))
n_layers = 4
for i in range(n_layers):
	# add layer
	add_layer(i, num_filters_per_hidden_layer, model, train_noisy, train_clean, validation_noisy, validation_clean)

print("Getting CNN output for noisy test set input...")
test_filtered_frames = (train_standard_deviation * cnn.get_output_multiple_batches(model, test_noisy)) + train_mean

print("Perfectly reconstructing filtered test set audio & saving to memory...")
test_filtered =pro.reconstruct( test_filtered_frames )
scipy.io.wavfile.write( filename = parent_cwd + "/Audio_Files/Test_Files/" + model_name + "_FilteredTest_" + str(snr_db) + "dB_1min.wav", rate = sampling_rate, data = test_filtered[0:(60*sampling_rate)])

summary_stats_filename = parent_cwd + "/Saved_Models/Model_Descriptions.txt"
cnn.summary_statistics( summary_stats_filename, model_name, frame_time, snr_db,
						 	num_filters_per_hidden_layer, filter_size_per_hidden_layer, filter_size_output_layer,
						 	epochs, batch_size)
